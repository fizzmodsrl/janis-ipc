<?php

use PHPUnit\Framework\TestCase;

use Janis\Ipc\Manager;
use Janis\Ipc\Request;

class RequestTest extends TestCase {

	private $serviceName = "test";
	private $namespace = "test";
	private $serviceMethod = "list";

	public function testCronstructCreatesUuid() {

		$request = new Request($this->serviceName, $this->namespace, $this->serviceMethod);

		$this->assertStringMatchesFormat("%x%x%x%x%x%x%x%x-%x%x%x%x-%x%x%x%x-%x%x%x%x-%x%x%x%x%x%x%x%x%x%x%x%x", $request->id);
	}

	public function testSetGetHeaders() {

		$headers = [
			"foo" => "bar"
		];

		$request = new Request($this->serviceName, $this->namespace, $this->serviceMethod);

		$request->setHeaders($headers);

		$this->assertSame($headers, $request->getHeaders());

		$newHeaders = [
			"baz" => "yeah"
		];

		$request->setHeaders($newHeaders, true);

		$allHeaders = [
			"foo" => "bar",
			"baz" => "yeah"
		];

		$this->assertSame($allHeaders, $request->getHeaders());

		$newHeaders = [
			"baz" => "yeah"
		];

		$request->setHeaders($newHeaders);

		$this->assertSame($newHeaders, $request->getHeaders());
	}

	public function testSetGetData() {

		$data = [
			"foo" => "bar",
			"baz" => [
				"test" => "yeah"
			]
		];

		$request = new Request($this->serviceName, $this->namespace, $this->serviceMethod);

		$request->setData($data);

		$this->assertSame($data, $request->getData());
	}

	public function testSetGetUrlParts() {

		$UrlParts = [
			"foo" => "bar"
		];

		$request = new Request($this->serviceName, $this->namespace, $this->serviceMethod);

		$request->setUrlParts($UrlParts);

		$this->assertSame($UrlParts, $request->getUrlParts());
	}

	public function testDefaultMethod() {

		$request = new Request($this->serviceName, $this->namespace, $this->serviceMethod);

		$this->assertSame("GET", $request->getMethod());
	}

	public function testSetGetMethod() {

		$method = "POST";

		$request = new Request($this->serviceName, $this->namespace, $this->serviceMethod);
		$request->setMethod($method);

		$this->assertSame($method, $request->getMethod());
	}

	public function testSetInvalidMethod() {

		$request = new Request($this->serviceName, $this->namespace, $this->serviceMethod, "INVALIDMETHOD");

		$this->assertSame("GET", $request->getMethod());
	}

	public function testJsonEncoding() {

		$headers = ["Content-Type" => "application/json"];
		$data = ["foo" => "bar"];

		$request = new Request($this->serviceName, $this->namespace, $this->serviceMethod, "POST");
		$id = $request->id;

		$request->setHeaders($headers);
		$request->setData($data);

		$jsonEncoded = json_encode($request);
		$jsonDecoded = json_decode($jsonEncoded, true);

		$this->assertArraySubset([
			"id" => $id,
			"service" => $this->serviceName,
			"namespace" => $this->namespace,
			"method" => $this->serviceMethod,
			"verb" => "POST",
			"headers" => $headers,
			"data" => $data
		], $jsonDecoded);
	}

}

?>