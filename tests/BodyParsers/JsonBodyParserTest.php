<?php

use PHPUnit\Framework\TestCase;

use Janis\Ipc\BodyParsers\JsonBodyParser;

class JsonBodyParserTest extends TestCase {

	public function testParseInvalidJson() {

		$bodyParser = new JsonBodyParser();
		$parsedBody = $bodyParser->parse("invalid [ Json");

		$this->assertNull($parsedBody);
	}

	public function testParseValidJson() {

		$expectedBody = (object)[
			"foo" => "bar",
			"baz" => ["yeah", 100, 3]
		];

		$bodyParser = new JsonBodyParser();
		$parsedBody = $bodyParser->parse(json_encode($expectedBody));

		$this->assertEquals($expectedBody, $parsedBody);
	}
}

?>