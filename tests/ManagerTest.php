<?php

use PHPUnit\Framework\TestCase;

use Janis\Ipc\Manager;
use Janis\Ipc\Request;

class ManagerTest extends TestCase {

	private $gatewayUrl = "http://localhost:3005";

	private $serviceName = "test";
	private $namespace = "test";
	private $serviceMethod = "list";

	public function testSubscribe() {

		$dummySubscribePath = realpath(__DIR__ ."/schemas/test-schema.json");

		$expectedSubscribeUrl = sprintf("%s/subscribe/%s", $this->gatewayUrl, $this->serviceName);

		$curlMock = $this->createMock("\Curl\Curl");
		$curlMock->expects($this->once())
			->method("post")
			->with($expectedSubscribeUrl, file_get_contents($dummySubscribePath));

		$curlMock->http_status_code = 200;
		$curlMock->response = "publicKey";

		$manager = new Manager($this->gatewayUrl, $this->serviceName, "", $curlMock);

		$this->assertTrue($manager->subscribe($dummySubscribePath));
	}

	public function testAddRequests() {

		$dummySubscribePath = __FILE__;
		$manager = new Manager($this->gatewayUrl);

		$requests = [];

		$request = new Request($this->serviceName, $this->namespace, $this->serviceMethod);
		$requests[] = $request;

		$request = new Request($this->serviceName, $this->namespace, $this->serviceMethod);
		$requests[] = $request;

		$res = $manager->addRequests($requests);

		$this->assertInternalType("array", $res);
	}

	public function testCall() {

		$callResponses = json_encode([
			(object)["foo" => "bar"]
		]);

		$request = new Request($this->serviceName, $this->namespace, $this->serviceMethod);

		$expectedCallUrl = sprintf("%s/call", $this->gatewayUrl);

		$curlMock = $this->createMock("\Curl\Curl");
		$curlMock->expects($this->once())
			->method("post")
			->with($expectedCallUrl, json_encode([$request]));

		$curlMock->response = $callResponses;

		$dummySubscribePath = __FILE__;

		$manager = new Manager($this->gatewayUrl, $this->serviceName, "", $curlMock);
		$manager->addRequest($request);

		$response = $manager->call();
		$this->assertNotNull($response);
	}

	public function testSetSchemaInvalid() {

		// Empty schema
		$schemaPath = "";
		$manager = new Manager($this->gatewayUrl, $this->serviceName);
		$res = $manager->setSchema($schemaPath);
		$this->assertFalse($res);

		// Empty trimmed schema
		$schemaPath = " ";
		$manager = new Manager($this->gatewayUrl, $this->serviceName);
		$res = $manager->setSchema($schemaPath);
		$this->assertFalse($res);

		// Wrong schems
		$schemaPath = "./wrong/schema/path";
		$manager = new Manager($this->gatewayUrl, $this->serviceName);
		$res = $manager->setSchema($schemaPath);

		$this->assertFalse($res);
	}

	public function testSetSchemaValid() {

		$schemaPath = realpath(__DIR__ ."/schemas/test-schema.json");

		$manager = new Manager($this->gatewayUrl, $this->serviceName);
		$res = $manager->setSchema($schemaPath);

		$className = "\\Janis\\Ipc\\Manager";
		$this->assertInstanceOf($className, $res);
	}

	public function testGetAuthenticationTokenWithoutAuth() {

		if(isset($_SERVER["Authorization"])) unset($_SERVER["Authorization"]);

		$manager = new Manager($this->gatewayUrl, $this->serviceName);
		$token = $manager->getAuthenticationToken();

		$this->assertNull($token);
	}

	public function testGetAuthenticationTokenWithInvalidAuth() {

		$_SERVER["Authorization"] = "NotValid TokenTest";

		$manager = new Manager($this->gatewayUrl, $this->serviceName);
		$token = $manager->getAuthenticationToken();

		$this->assertNull($token);
	}

	public function testGetAuthenticationTokenWithAuth() {

		$_SERVER["Authorization"] = "Bearer TokenTest";

		$manager = new Manager($this->gatewayUrl, $this->serviceName);
		$token = $manager->getAuthenticationToken();

		$this->assertSame($token, "TokenTest");
	}

	public function testAuthenticateWithoutAuth() {

		if(isset($_SERVER["Authorization"])) unset($_SERVER["Authorization"]);

		$manager = new Manager($this->gatewayUrl, $this->serviceName);
		$authResult = $manager->authenticate();

		$this->assertFalse($authResult);
	}

	public function testAuthenticateWithInvalidAuth() {

		$_SERVER["Authorization"] = "NotValid TokenTest";

		$manager = new Manager($this->gatewayUrl, $this->serviceName);
		$authResult = $manager->authenticate();

		$this->assertFalse($authResult);
	}

	public function testAuthenticateWithInvalidToken() {

		$_SERVER["Authorization"] = "Bearer TokenTest";

		$manager = new Manager($this->gatewayUrl, $this->serviceName);
		$authResult = $manager->authenticate();

		$this->assertFalse($authResult);
	}

	/**
	 * Valida la autenticación con datos correctos
	 *
	 * @dataProvider validAuthDataProvider
	 *
	 * @param string $token The token
	 * @param string $publicKey The public key
	 */
	public function testAuthenticateWithValidToken($token, $publicKey) {

		$manager = new Manager($this->gatewayUrl, $this->serviceName);
		$authResult = $manager->authenticate("", $token, $publicKey);

		$this->assertTrue($authResult);
	}

	public function testGetEndpoint() {

		$expectedUrl = sprintf("%s/api/1/endpoint", $this->gatewayUrl);
		$dummyEndpointWithParameters = "http://example.com/api/foo/bar/{fooId}";
		$expectedEndpoint = "http://example.com/api/foo/bar/5";

		$type = "api";
		$serviceName = $this->serviceName;
		$namespace = "myNamespace";
		$method = "myMethod";

		$curlMock = $this->createMock("\Curl\Curl");
		$curlMock->expects($this->once())
			->method("get")
			->with($expectedUrl, [
				"type" => $type,
				"ms" => $serviceName,
				"ns" => $namespace,
				"method" => $method
			]);

		$curlMock->http_status_code = 200;
		$curlMock->response = $dummyEndpointWithParameters;

		$manager = new Manager($this->gatewayUrl, $this->serviceName, "", $curlMock);
		$endpoint = $manager->getEndpoint($type, $serviceName, $namespace, $method, [
			"fooId" => 5
		]);

		$this->assertSame($expectedEndpoint, $endpoint);
	}

	public function validAuthDataProvider() {
		return [
			"Valid authentication 1" => [
				"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJKYW5pcy1DbGllbnQiOiJmaXp6bW9kYXJnIiwiaWF0IjoxNTA5NDc2MjQxfQ.dEPMRrVEpyM9jKQGq8GLpMScozsDBX1LxVMJTns3Vtu8k3YZgOQkbVt4BjFtVCvtjnSx7Jr_VVGPvmUvLw8PxEeBVjvK0zuXy-7eFUndEdYwDAFbLPQKS2zMYKBQ8ZvYTd5Re0FdvLm_fB-hOy_bLcGl7dgt55CS4wAN7NQTry-sSeJTVr4xA9Dnc8JOY4SDMofSgjZBXQPPKw7wWoB1Rc6nT6R17H_XAWBOIXv2FPHK1TVw7w5AhD5CgST6tw2PjJT-Lp_FkiDV55HzWZ6QNzn-P7yk8yEQkif9YbYdphnE34QPYd4CkyFU7kejm9P714-mQn5BfIK1NU-0R5aG1-laFN909HACmkp8WYdQ2U7bRCbo9xNRtZUL2uFqjIiB5K0Jeqv-ZZklf40UIHnA-6WOjXdMRMAn23cO0vDvRqct4TQvw9VT-bav2e_fTSreMJX-JqPv3bzYLiYzaD78YfyhNRm4ajIklF6zwVi3afADOBiOO0bI46YNOVFSC09W3_bT8UNfGJrUvAcVA5_eCnswlQebP6Ls-xAu1JnFUjrh4WilTjdRixqAV1kOTWgdwEhs1G0_qHGzQ4Dt-IOW5UAfZgjHxZYAmJFSrwnLEh0vBrIJu-ihMO6FsYWC-KfltzAWb0JaffqHoNhVGfU9PaOxP-Dns-pdVPFbKrfoF-U",
				"-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAvvuRyg5TsKTGXwoO05Ck
UIs5V8P5sJPKHM54msjwsr3hFJv3VWpAoBjo1snFQkMTRg1BDTSpZnI7es2D+5ZW
LANX/H/WWH6kuxfb9sVo8N7wVdcchE3gf/Ej1aIUnGF6BvNY84BFb/K/uUbh/9RX
EHTU01ei4Mr7US5MY5ljtE3GdWVZQFr4TEdVXEfrTzNvhogVpsAATWlsRaN11TpW
S1l55IGyhrba36i1PREmuDtYnaoIvnqPPUFlC86xOcRBoqF1m3/c6TWOp+DiPCP3
ZqtcGsBgda5XCGp9SUlRjjhc+REReM1V6I7YNS43yx7KDDT8X+bEsM5mAfoYZSgN
YL5dV/J3QcLO6D975EHKQAKLvQ5sc1NU22feenIM0dEJbfnFmMufqJDP7DgW18ZG
eiMvUTfFPwCCYwnp/r0+vwzOllccJkSeINoYd0n46VEbvHZHMjmcLZlDye1wb82b
HGO9mZC2qxXfy0NOJ/JJJly3p5JtAJF3A1yDjJq7FUQwAx+vWz3jSq51Sawcu+E6
QOE61qeayS+uqaIwZvPcJoU792nmvz/ragRftoa99MdTt1RFGJ0+717vSxCzgaTr
6AB6yXDjNZ7xbDE0oQOSNj9/85+5+AsTvqxQMbV3I4iHKi6bFD4CyNYAZDErQirL
IFvC2Bps9RO8drSsbQHscIkCAwEAAQ==
-----END PUBLIC KEY-----"
			]
		];
	}

}

?>