<?php

namespace Janis\Ipc\BodyParsers;

/**
 * The class to manage IPC Gateway communication
 */
class JsonBodyParser implements BodyParserInterface {

	public function parse($body) {
		return json_decode($body);
	}

}

?>