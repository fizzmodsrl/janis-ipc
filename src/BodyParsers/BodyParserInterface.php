<?php

namespace Janis\Ipc\BodyParsers;

/**
 * The interface to implement a IPC response body parser
 */
interface BodyParserInterface {

	public function parse($body);

}

?>