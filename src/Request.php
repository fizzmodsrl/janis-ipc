<?php

namespace Janis\Ipc;

use \Ramsey\Uuid\Uuid;

/**
 * The class to implement a new call
 */
class Request implements \JsonSerializable {

	/**
	 * The service name of the request
	 *
	 * @var String $serviceName
	 */
	private $serviceName = "";

	/**
	 * The namespace of the request
	 *
	 * @var String $namespace
	 */
	private $namespace = "";

	/**
	 * The service method of the request
	 *
	 * @var String $serviceMethod
	 */
	private $serviceMethod = "";

	/**
	 * The request method
	 *
	 * @var string $method
	 */
	private $method = "GET";

	/**
	 * The allowed methods
	 *
	 * @var array $allowedMethods
	 */
	private $allowedMethods = ["GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"];

	/**
	 * The headers of the request
	 *
	 * @var Array $headers
	 */
	private $headers = [];

	/**
	 * The request data
	 *
	 * @var mixed $data
	 */
	private $data = null;

	/**
	 * The class constructor.
	 *
	 * @param string $serviceName The service name
	 * @param string $namespace The namespace
	 * @param string $serviceMethod The service method
	 * @param string $httpMethod The HTTP method
	 */
	public function __construct($serviceName, $namespace, $serviceMethod, $httpMethod = "") {

		$this->id = Uuid::uuid4()->toString();

		$this->serviceName = $serviceName;
		$this->namespace = $namespace;
		$this->serviceMethod = $serviceMethod;

		if(!empty($httpMethod)) $this->setMethod($httpMethod);
	}

	/**
	 * Sets the method.
	 *
	 * @param string $method The method
	 */
	public function setMethod($method) {
		$method = strtoupper(trim($method));
		if(in_array($method, $this->allowedMethods)) $this->method = $method;
	}

	/**
	 * Gets the method.
	 *
	 * @return string The method.
	 */
	public function getMethod() {
		return $this->method;
	}

	/**
	 * Sets the headers.
	 *
	 * @param array $headers The headers
	 * @param boolean $merge Whether to merge or override headers
	 */
	public function setHeaders($headers, $merge = false) {
		$this->headers = $merge ? array_merge_recursive($this->headers, $headers) : $headers;
	}

	/**
	 * Gets the headers.
	 *
	 * @return array The headers.
	 */
	public function getHeaders() {
		return $this->headers;
	}

	/**
	 * Sets the data.
	 *
	 * @param mixed $data The data
	 */
	public function setData($data) {
		$this->data = $data;
	}

	/**
	 * Gets the data.
	 *
	 * @return mixed The data.
	 */
	public function getData() {
		return $this->data;
	}

	/**
	 * Sets the url parts.
	 *
	 * @param mixed $urlParts The url parts
	 */
	public function setUrlParts($urlParts) {
		$this->urlParts = $urlParts;
	}

	/**
	 * Gets the url parts.
	 *
	 * @return mixed The url parts.
	 */
	public function getUrlParts() {
		return $this->urlParts;
	}

	/**
	 * Returns the data that is available when serializing an object to JSON
	 * @see http://php.net/manual/es/jsonserializable.jsonserialize.php
	 *
	 * @return array The object JSON representation
	 */
	public function jsonSerialize() {

		$jsonData = [
			"id" => $this->id,
			"service" => $this->serviceName,
			"namespace" => $this->namespace,
			"method" => $this->serviceMethod,
			"verb" => $this->method
		];

		if(isset($this->data)) $jsonData["data"] = $this->data;
		if(!empty($this->headers)) $jsonData["headers"] = $this->headers;
		if(!empty($this->urlParts)) $jsonData["parts"] = $this->urlParts;

		return $jsonData;
	}

}

?>