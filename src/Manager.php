<?php

namespace Janis\Ipc;

use \Curl\Curl;

use \Firebase\JWT\JWT;

/**
 * The class to manage IPC Gateway communication
 */
class Manager {

	/**
	 * The public key path template
	 *
	 * @const string PUBLIC_KEY_PATH_TEMPLATE
	 */
	const PUBLIC_KEY_PATH_TEMPLATE = "/tmp/janis-ipc-public-key-{serviceName}.key";

	/**
	 * The client store name for IPC Gateway originated requests
	 *
	 * @var string $clientStoreName
	 */
	private $clientStoreName = "";

	/**
	 * The URL where the IPC Gateway is implemented
	 *
	 * @var string	$gatewayUrl
	 */
	private $gatewayUrl = "";

	/**
	 * The pending requests
	 *
	 * @var array $requests
	 */
	private $requests = [];

	/**
	 * The service name
	 *
	 * @var string $serviceName
	 */
	private $serviceName = "";

	/**
	 * The schema file path
	 *
	 * @var string $schemaFilePath
	 */
	private $schemaFilePath = "";

	/**
	 * The public keys from de IPC
	 *
	 * @var string $publicKey
	 */
	private $publicKey = "";

	/**
	 * The possible JWT algorithms
	 *
	 * @var array $possibleAlgorithms
	 */
	private $possibleAlgorithms = ["RS256"];

	/**
	 * The responses body parser
	 *
	 * @var object $bodyParser
	 */
	private $bodyParser;

	/**
	 * The curl handler
	 *
	 * @var \Curl\Curl $curlHandler
	 */
	private $curlHandler;

	/**
	 * The class constructor
	 *
	 * @param string $gatewayUrl The gateway url
	 * @param string $serviceName The service name
	 *
	 * @return Janis\Ipc\Manager The manager object
	 *
	 * @throws Exception When @param $gatewayUrl is empty
	 */
	public function __construct($gatewayUrl, $serviceName = "", $bodyParser = "", Curl $curlHandler = null) {

		if(!is_string($gatewayUrl) || empty(trim($gatewayUrl))) throw new Exception("Gateway URL is required.");

		if(is_string($serviceName) && !empty(trim($serviceName))) $this->serviceName = $serviceName;

		if(empty($bodyParser) || !is_object($bodyParser)) $bodyParser = new BodyParsers\JsonBodyParser();
		$this->setBodyParser($bodyParser);

		$this->gatewayUrl = rtrim(trim($gatewayUrl), "/");

		$this->curlHandler = !empty($curlHandler) ? $curlHandler : new Curl();
	}

	/**
	 * Sets the schema.
	 *
	 * @param string $schemaFilePath The schema file path
	 *
	 * @return boolean|self The object instance, or false in case of error
	 */
	public function setSchema($schemaFilePath) {

		if(!is_string($schemaFilePath) || empty(trim($schemaFilePath)) || !is_readable($schemaFilePath)) return false;
		$this->schemaFilePath = $schemaFilePath;

		return $this;
	}

	/**
	 * Subscribes your service in the IPC Gateway,
	 *
	 * @param string $serviceName The service name
	 * @param string $schemaFilePath The path of your Open Api Specification schema file
	 *
	 * @return Boolean Indicates whether the service was subscribed or not
	 */
	public function subscribe($schemaFilePath = "", $serviceName = "") {

		$serviceName = is_string($serviceName) && !empty(trim($serviceName)) ? trim($serviceName) : $this->serviceName;
		$schemaFilePath = is_string($schemaFilePath) && !empty(trim($schemaFilePath)) ? trim($schemaFilePath) : $this->schemaFilePath;

		if(!is_string($serviceName) || empty(trim($serviceName))) return false;
		if(!is_string($schemaFilePath) || empty(trim($schemaFilePath)) || !is_readable($schemaFilePath)) return false;

		$this->schemaFilePath = $schemaFilePath;

		$serviceName = trim($serviceName);

		$subscribeUrl = sprintf("%s/subscribe/%s", $this->gatewayUrl, $serviceName);
		$schema = file_get_contents(trim($schemaFilePath));

		$this->curlHandler->setOpt(CURLOPT_RETURNTRANSFER, true);
		$this->curlHandler->setHeader("Content-Type", "application/json");
		$this->curlHandler->setOpt(CURLOPT_CONNECTTIMEOUT, 10);
		$this->curlHandler->setOpt(CURLOPT_TIMEOUT, 10);
		$this->curlHandler->post($subscribeUrl, $schema);
		$this->curlHandler->close();

		if($this->curlHandler->error) return false;

		if($this->curlHandler->http_status_code !== 200) return false;

		$this->publicKey = $this->curlHandler->response;

		$persisted = $this->persistPublickKey($serviceName, $this->publicKey);

		return $persisted;
	}

	/**
	 * Persists IPC validation public key
	 *
	 * @param string $serviceName The service name
	 * @param string $publicKey The public key
	 *
	 * @return Boolean Indicates whether the key was persisted or not
	 */
	protected function persistPublickKey($serviceName, $publicKey) {

		$publicKeyPath = str_replace("{serviceName}", $serviceName, self::PUBLIC_KEY_PATH_TEMPLATE);

		$persisted = @file_put_contents($publicKeyPath, $publicKey);

		return !empty($persisted);
	}

	/**
	 * Exectutes all pending requests,
	 *
	 * @return array Every pending request responses
	 */
	public function call() {

		if(empty($this->requests)) return false;
		if(empty($this->gatewayUrl)) return false;

		$response = $this->makeRequests($this->requests);

		$this->requests = [];

		return $response;
	}

	/**
	 * Adds a new request to the call stack,
	 *
	 * @param Janis\Ipc\Request $request The request object to add
	 *
	 * @return An ID for the request
	 */
	public function addRequest(Request $request) {
		return array_push($this->requests, $request);
	}

	/**
	 * Adds an array of requests to the call stack,
	 *
	 * @see Janis\Ipc\Manager::addRequest()
	 *
	 * @param array $requests A list of Janis\Ipc\Request's to add
	 *
	 * @return array A list of IDs for the requests, maintaining indexes
	 */
	public function addRequests($requests) {

		return array_map(function($request) {
			return $this->addRequest($request);
		}, $requests);
	}

	/**
	 * Performs the call to the IPC Gateway with multiple requests
	 *
	 * @param array $requests The list of pending requests
	 *
	 * @return array The responses
	 */
	private function makeRequests($requests) {

		$this->curlHandler->setOpt(CURLOPT_RETURNTRANSFER, true);
		$this->curlHandler->setHeader("Content-Type", "application/json");
		$this->curlHandler->post($this->gatewayUrl ."/call", json_encode($requests));
		$this->curlHandler->close();

		if($this->curlHandler->error) {
			return null;
		}

		$responses = json_decode($this->curlHandler->response);
		if(empty($responses)) $responses = [];

		$bodyParser = $this->getBodyParser();

		if(!empty($bodyParser)) {
			$responses = array_map(function($response) use($bodyParser) {
				if(isset($response->body)) $response->body = $bodyParser->parse($response->body);
				return $response;
			}, $responses);
		}

		return $responses;
	}

	/**
	 * Gets the body parser.
	 *
	 * @return object The body parser.
	 */
	public function getBodyParser() {
		return $this->bodyParser;
	}

	/**
	 * Sets the body parser.
	 *
	 * @param object $bodyParser The body parser class name
	 */
	public function setBodyParser($bodyParser) {

		$this->bodyParser = $bodyParser;
	}

	/**
	 * Gets the authentication token.
	 *
	 * @return string|null The authentication token, or null in case of error
	 */
	public function getAuthenticationToken() {

		if(!empty($_SERVER["Authorization"])) $token = $_SERVER["Authorization"];
		elseif(function_exists("getallheaders")) {

			$headers = getallheaders();
			if(!empty($headers["Authorization"])) $token = $headers["Authorization"];
		}

		if(empty($token) || stripos($token, "Bearer ") === false) return null;
		return trim(str_ireplace("Bearer ", "", $token));
	}

	/**
	 * Authenticates against JWT (Json Web Token)
	 *
	 * @param string $serviceName The service name
	 * @param string $token The token
	 * @param string $publicKey The public key
	 *
	 * @return boolean Indicate whether authentication is valid or not
	 */
	public function authenticate($serviceName = "", $token = "", $publicKey = "") {

		if(empty($token)) $token = $this->getAuthenticationToken();
		if(empty($token)) return false;

		$serviceName = is_string($serviceName) && !empty(trim($serviceName)) ? trim($serviceName) : $this->serviceName;
		if(empty($serviceName)) return false;

		$publicKey = is_string($publicKey) && !empty(trim($publicKey)) ? trim($publicKey) : $this->getPublicKey();
		if(empty($publicKey)) return false;

		try {
			$jwtDecoded = JWT::decode($token, $publicKey, $this->possibleAlgorithms);
		} catch(\Exception $e) {
			$jwtDecoded = false;
		}

		if(empty($jwtDecoded)) {

			if($this->subscribe()) {

				try {
					$jwtDecoded = JWT::decode($token, $publicKey, $this->possibleAlgorithms);
				} catch(\Exception $e) {
					$jwtDecoded = false;
				}

			}

		}

		if(empty($jwtDecoded)) return false;
		$jwtDecoded = (array)$jwtDecoded;

		$this->clientStoreName = !empty($jwtDecoded["Janis-Client"]) ? $jwtDecoded["Janis-Client"] : false;

		return !empty($this->clientStoreName);
	}

	/**
	 * Gets the public key.
	 *
	 * @param string $serviceName The service name
	 *
	 * @return string The public key.
	 */
	protected function getPublicKey($serviceName = "") {

		$serviceName = is_string($serviceName) && !empty(trim($serviceName)) ? trim($serviceName) : $this->serviceName;
		if(empty($serviceName)) return false;

		if(empty($this->publicKey)) {

			$publicKeyPath = str_replace("{serviceName}", $serviceName, self::PUBLIC_KEY_PATH_TEMPLATE);

			if(is_readable($publicKeyPath)) {
				$this->publicKey = file_get_contents($publicKeyPath);
			}

		}

		return $this->publicKey;
	}

	public function getEndpoint($type, $serviceName, $namespace, $method, $parameters = []) {

		$serviceName = trim($serviceName);

		$endpointUrl = sprintf("%s/api/1/endpoint", $this->gatewayUrl);

		$urlParams = [
			"type" => $type,
			"ms" => $serviceName,
			"ns" => $namespace,
			"method" => $method
		];

		$this->curlHandler->setOpt(CURLOPT_RETURNTRANSFER, true);
		$this->curlHandler->setHeader("Content-Type", "application/json");
		$this->curlHandler->setOpt(CURLOPT_CONNECTTIMEOUT, 10);
		$this->curlHandler->setOpt(CURLOPT_TIMEOUT, 10);
		$this->curlHandler->get($endpointUrl, $urlParams);
		$this->curlHandler->close();

		if($this->curlHandler->error) return null;

		if($this->curlHandler->http_status_code !== 200) return null;

		$endpoint = $this->curlHandler->response;

		foreach($parameters as $parameter => $value) {
			$endpoint = str_replace(sprintf("{%s}", $parameter), $value, $endpoint);
		}

		return $endpoint;
	}

}

?>